import newCanvas from './engine/canvas';
import './style.css';

function component() {
    const element = document.createElement('div');
    const btnCanvas = document.createElement('button');

    element.classList.add('grid');

    btnCanvas.textContent = 'Canvas';
    btnCanvas.classList.add('btn-canvas');

    btnCanvas.addEventListener('click', newCanvas);

    element.appendChild(btnCanvas);

    return element;
}

document.body.appendChild(component());