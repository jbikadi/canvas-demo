// return a random number
export function random(min: number, max: number) {
    return min + Math.random() * (max - min);
}

export function randomColor() {
    return `hsla(${random(0, 360)} 100% 75% / 0.75)`;
}