import {random, randomColor} from './utilities';

// generates a new canvas on screen
export default function newCanvas() {
    const body = document.querySelector('body');
    const width = 250;
    const height = 250;
    const canvas = document.createElement('canvas');
    const ctx = canvas.getContext('2d');

    canvas.width = width;
    canvas.height = height;
    canvas.classList.add('canvas');
    canvas.style.top = random(0, window.innerHeight - height) + 'px';
    canvas.style.left = random(0, window.innerWidth - width) + 'px';

    // Context (ctx) manipulation
    for (let i = 0; i < 3; i++) {
        // drawing squares/rectangles
        const size = random(20, 70);
        const x = random(40, width - 40) - size / 2;
        const y = random(40, height - 40) - size / 2;
        ctx.strokeStyle = randomColor();
        ctx.lineWidth = random(2, 7);

        // save point canvas current rotation
        ctx.save();
        ctx.translate(width / 2, height / 2);
        // 180 degrees = 1pi radius
        ctx.rotate(random(0, Math.PI * 0.5));
        ctx.translate(width / -2, height / -2);
        ctx.strokeRect(x, y, size, size);

        // restore canvas rotation to previous save point
        ctx.restore();

        // drawing triangles
        ctx.beginPath();
        ctx.moveTo(random(0, width), random(0, height));
        ctx.lineTo(random(0, width), random(0, height));
        //ctx.arc(120, 120, 60, 1, Math.PI / 2);
        ctx.lineTo(random(0, width), random(0, height));
        ctx.closePath();
        ctx.strokeStyle = randomColor();
        ctx.lineWidth = random(2, 7);
        ctx.stroke();

        // drawing circles
        ctx.beginPath();
        ctx.arc(random(40, width - 40), random(40, height - 40), random(10, 40), 0, Math.PI * 2);
        ctx.strokeStyle = randomColor();
        ctx.lineWidth = random(2, 7);
        ctx.stroke();
    }

    // fonts on canvas adjust the alignments the styles and fill and stroke
    ctx.font = '30px system-ui';
    ctx.textAlign = 'center';
    ctx.textBaseline = 'middle';
    ctx.fillStyle = 'white';
    ctx.strokeStyle = 'black';
    ctx.lineWidth = 4;
    ctx.strokeText('!canvas', width/2, height/2);
    ctx.fillText('!canvas', width/2, height/2);



    body.appendChild(canvas);

    setTimeout(() => {
        body.removeChild(canvas)
    }, 3000);
}