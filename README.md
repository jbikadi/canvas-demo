
## Getting Started

```bash
npm i

npm run dev

```

browser should open on [http://localhost:8080/](http://localhost:8080/)

## Learn More

Based on Amit Sheen and Kevin Powell
Source: [https://www.youtube.com/watch?v=OXWY19UHXzc](https://www.youtube.com/watch?v=OXWY19UHXzc)
Amit Sheen: [https://codepen.io/amit_sheen/](https://codepen.io/amit_sheen/)
Kevin Powell: [https://www.youtube.com/@KevinPowell](https://www.youtube.com/@KevinPowell)